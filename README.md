# karma-android-device-browser-launcher

> Run Karma tests on the specified android webview application. Adb must be registered on path to communicate with device or emulator.

## Installation

Dependency `karma-android-webview-launcher` must be declared in your dependency manager. Kotlin JS build.gradle exemple :
```gradle
dependencies {
    ...
    // TU Karma
    testImplementation "org.jetbrains.kotlin:kotlin-test-js"
    testImplementation api(npm("puppeteer", "*")) // Chrome headless
    testImplementation npm('karma-android-webview-launcher','^1.0.4')
}
```

## Usage
```js
// karma.conf.js
config.set({
    logLevel: config.LOG_DEBUG,
    browsers: ['ChromeHeadless', 'AppNavigateurWeb'],
    customLaunchers: {
      AppNavigateurWeb: {
        base: 'AndroidWebView',
        androidAppPackage: 'com.test.app',
        androidAppActivity: '.ui.MainActivity'
        //, adbDeviceId: 'emulator-5554' /* -> can be ommited to use connected device */
      }
    }
});
```


## Android side
```kotlin
// In functions
    override fun onCreate(savedInstanceState: Bundle?)
    override fun onNewIntent(intent: Intent?)

// use URI in intent data
   val uri = intent?.data 

// to redirect webview to karma test page
mywebview.load(uri)
```

Have fun