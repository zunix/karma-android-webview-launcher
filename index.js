var spawn = require('child_process').spawn

var adb = 'adb';
var nomLogger = 'launcher.android_device';

// Implémentation de test Karma du navigateur infomil sur un terminal (ou émulateur) Android
var AndroidWebView = function(args, logger, baseLauncherDecorator) {
    baseLauncherDecorator(this)

    var log = logger.create(nomLogger);
    var adbDeviceId = args.adbDeviceId || null;
    var androidAppPackage = args.androidAppPackage || null;
    var androidAppActivity = args.androidAppActivity || null;
    var killingPromise = null;

    // Surcharge méthode démarrage du plugin Karma
    this.start = function(url) {
        this._verifierArguments();
        this._activerReverseTcpLocalhost(url);
        this._demarrerNavigateur(url);
    }

    // Vérifier la validité des arguments
    this._verifierArguments = function() {
        if (this._isNullOrEmpty(androidAppPackage)) {
            log.error('Package Android null ou vide');
            throw new Exception('Package Android null ou vide');
        }

        if (this._isNullOrEmpty(androidAppActivity)) {
            log.error('Activité Android nulle ou vide');
            throw new Exception('Activité Android nulle ou vide');
        }
    }

    // Vérifier si une chaine est nulle ou vide
    this._isNullOrEmpty = function(value) {
        return (value == null || value === '');
    }

    // Activer le port forwarding adb sur le terminal
    this._activerReverseTcpLocalhost = function(url) {
        var testUrl = new URL(url);
        if (testUrl.hostname == 'localhost') {
            var optionsTcpReverse = this._getAdbArgumentsTcpReverse(testUrl.port);
            log.debug('Activation mode port forwarding localhost du port %s', testUrl.port);
            this._execCommand(adb, optionsTcpReverse);
        }
    }

    // Exécuter une commande sut l'OS hôte
    this._executerCommand = function(cmd, args, closeCallback = null) {
        if (!cmd) {
            log.error('ADB non trouvé dans le PATH');
            this._retryLimit = -1
            return this._clearTempDirAndReportDone('no binary')
        }

        cmd = this._normalizeCommand(cmd)
        log.debug("Exécution commande %s %s", cmd, args.join(' '));
        this._process = spawn(cmd, args)

        this._process.on('close', function(code) {
            log.debug(cmd + ' terminé')
            if (closeCallback) {
                closeCallback()
            }
        })

        this._process.on('error', function(err) {
            if (err.code === 'ENOENT') {
                this._retryLimit = -1
                errorOutput = 'Impossible de trouver le binaire ' + cmd + '\n\t' + 'Introuvable dans le PATH'
            } else {
                errorOutput += err.toString()
                log.error("Erreur: " + errorOutput);
            }
        })
    }

    // Obtenir les arguments du mode reverse ports ADB
    this._getAdbArgumentsTcpReverse = function(tcpPort) {
        return [
            (adbDeviceId === null) ? null : '-s',
            (adbDeviceId === null) ? null : adbDeviceId,
            'reverse',
            'tcp:' + tcpPort,
            'tcp:' + tcpPort
        ].filter(n => n);
    }

    // Démarrer le navigateur sur le terminal
    this._demarrerNavigateur = function(url) {
        var urlKarma = url + '?id=' + this.id;
        var optionsExecutionNavigateur = this._getAdbArgumentsLancementNavigateur(urlKarma)
        log.debug('Démarrage de %s id[%s] and options %s', this.name, this.id, optionsExecutionNavigateur);
        this._executerCommand(adb, optionsExecutionNavigateur);
    }

    // Obtenir les arguments du lancement du navigateur
    this._getAdbArgumentsLancementNavigateur = function(url) {
        return [
            (adbDeviceId === null) ? null : '-s',
            (adbDeviceId === null) ? null : adbDeviceId,
            'shell',
            'am',
            'start',
            '-a',
            'android.intent.action.VIEW',
            '-n',
            androidAppPackage + '/' + androidAppActivity,
            '-d "' + url + '"'
        ].filter(n => n);
    }

    // Surcharge Karma restart
    this.restart = function() {
        log.debug('Rédémarrage %s', this.name)
    }

    // Surcharge Karma Kill, pour fermer le navigateur
    this.kill = function() {
        log.debug('Processus %s terminé', this.name)

        if (killingPromise) {
            return killingPromise
        }

        var self = this
        var adbForceStop = this._getAdbArgumentsFermetureNavigateur()
        killingPromise = this.emitAsync('android-device-browser-kill')
            .then(function(resolve, reject) {
                self._executerCommand(adb, adbForceStop, function() {
                    self._done()
                })
            });

        return killingPromise;
    }

    // Obtenir les arguments de fermeture du navigateur
    this._getAdbArgumentsFermetureNavigateur = function() {
        return [
            (adbDeviceId === null) ? null : '-s',
            (adbDeviceId === null) ? null : adbDeviceId,
            'shell',
            'am',
            'force-stop',
            androidAppPackage
        ].filter(n => n);
    }

    // Surcharge Karma
    this.forceKill = function() {
        log.debug('forceKill')
        return this.kill()
    }

    // Surcharge Karma
    this.markCaptured = function() {
        log.debug('markCaptured')
    }

    // Surcharge Karma
    this.isCaptured = function() {
        log.debug('isCaptured = true')
        return true
    }

    // Surcharge Karma
    this._done = function(error) {
        log.debug('Exécution terminée, envoi à karma avec status : %s', error)
        this.emit('done')

        if (this.error) {
            this.error = error
            emitter.emit('browser_process_failure', this)
        }
    }
}

AndroidWebView.prototype = {
    name: 'AndroidWebView',
    DEFAULT_CMD: {
        linux: '',
        darwin: '',
        win32: ''
    },
    ENV_CMD: 'ANDROIDDEVICE_BIN'
}

AndroidWebView.$inject = ['args', 'logger', 'baseBrowserDecorator'];

module.exports = {
    'launcher:AndroidWebView': ['type', AndroidWebView]
}